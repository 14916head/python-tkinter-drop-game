
# Use 1600 x 900 resolution
from tkinter import *
from tkinter import messagebox
from tkinter import constants as tk
from PIL import Image, ImageTk
from time import time
from copy import deepcopy
import math

# Constants

SAVE_FILE = "SaveFile.txt"

CREAM = "#F8F1DB"
GREEN = "#BDECC3"
BLUE  = "#B7B5FE"
PINK  = "#FDC3F1"

BUTTON_STYLE = {'height':2, 'width':40, 'relief':"solid", 'borderwidth':4,
                'activebackground':PINK, 'font':("Terminal", 16), 'bg':BLUE}

GRAVITY = 20
EPSILON = 6
BOUNCINESS = 1.25

ball_radius = 10
OFFSET_X = -15
OFFSET_Y = 30

DEFAULT_VAR_BANK = {
    'screen' : "Pause Menu",
    'vel_x'  : 50,
    'vel_y'  : 0,
    'state' : "Held",
    'hand_coords' : [50, 50],
    'goal_coords' : [450, 868],
    'goal_vel' : 200,
    'ball_coords' : [50 + OFFSET_X,
                     50 + OFFSET_Y,
                     50 + OFFSET_X + 2 * ball_radius,
                     50 + OFFSET_Y + 2 * ball_radius],
    'time' : 0,
    'continuable' : False
    }

L = 0
T = 1
R = 2
B = 3

X = 0
Y = 1

# Initialise
var_bank = {
    'screen' : "Pause Menu",
    }
settings = {}
hand_dir = ["_", "_"]



################################ VECTORS #####################################

def vect_mag(vect):
    return math.sqrt(vect[0] ** 2 + vect[1] ** 2)

def scal_x_vect(scal, vect):
    return [scal * coord for coord in vect]

def vect_add(vec1, vec2):
    if len(vec1) != len(vec2):
        print("Vectors", vec1, "and", vec2, "do not have same length")

    return [vec1[x] + vec2[x] for x in range(len(vec1))]

def vect_minus(vec1, vec2):
    if len(vec1) != len(vec2):
        print("Vectors", vec1, "and", vec2, "do not have same length")

    return [vec1[x] - vec2[x] for x in range(len(vec1))]

def vect_normalise(vect):
    mag = vect_mag(vect)
    return scal_x_vect(1 / mag, vect)

def vect_dot(vec1, vec2):
    # sum_buffer = 0
    # for x1, x2 in vec1, vec2:
    #     sum_buffer += x1 * x2
    # return vec1[X] * vec2[X] + vec1[Y] * vec2[Y]
    if len(vec1) != len(vec2):
        print("Vectors", vec1, "and", vec2, "do not have same length")

    return sum(vec1[x] * vec2[x] for x in range(len(vec1)))

def collide(velocity, normal):
    mag = vect_mag(normal)
    return 2 * vect_dot(normal, velocity) * normal / mag / mag



################################# SAVING #####################################

def saveGame():
    #print("start saveGame")
#    #print("Default:", DEFAULT_VAR_BANK)
    with open(SAVE_FILE, "w") as file:
        for key in var_bank:
            value = var_bank[key]

            if type(value) == list:
                #print("Oh Dear!")

                entry = "["

                for element in value:
                    elem_type = type(element)
                    #print(elem_type)
                    entry += (
                                str(element)   + ": " +
                                str(elem_type) + ", "
                             )

                entry = entry.rstrip(", ") # to not create empty last element
                entry += "]"
                #print("entry:", entry)


            elif type(value) == dict:
                # For the program to interpret this dict's values it will need
                # to store the data types of each value
                entry = "{"

                for dict_key in value:
                    dict_val = value[dict_key]
                    val_type = type(dict_val)
                    entry += (
                                dict_key + ": " + str(dict_val) + ": " +
                                str(val_type) + ", "
                             )

                entry = entry.rstrip(", ") # to not create empty last element
                entry += "}"


            else:
                entry = str(value)


            file.write(key + "=" + entry + "=" + str(type(value)) + "\n")
    #print("end saveGame")

def loadGame():
    #print("in func loadGame")
#    #print("Default:", DEFAULT_VAR_BANK)
    global var_bank
    with open(SAVE_FILE, "r") as file:
        entries = file.readlines()
        # print(entries)
        entries = [ var.rstrip("\n").split("=") for var in entries ]
        # #print("at mid of loadGame")
        var_bank = dictFromList(entries)
    return

def loadSettings():
    global settings
    with open("Settings.txt", "r") as file:
        entries = file.readlines()
        entries = [ var.rstrip("\n").split("=") for var in entries ]
        # #print("at mid of loadSettings")
        settings = dictFromList(entries)
    return

def dictFromList(list):
    #print("in func dictFromList")
#    #print("Default:", DEFAULT_VAR_BANK)
    dict_buffer = {}
    for i in range(0, len(list)):
        #print("entry to conv:", list[i])
        key   = list[i][0]
        value = list[i][1] # returns a string which needs to be cast
        obj   = list[i][2] # data type of value

        if obj == "<class 'str'>":
            pass
        elif obj == "<class 'int'>":
            value = int(value)
        elif obj == "<class 'float'>":
            value = float(value)
        elif obj == "<class 'bool'>":
            value = bool(value=="True")
        elif obj == "<class 'list'>":
            #print("value:", value)
            value = value.lstrip("[").rstrip("]").split(", ")
            value = [elem.replace("'","").split(": ") for elem in value]
            value = listFromList(value)
            # TODO
            # if int(elem) above then does not reload after boss key toggle
            #print("value:", value)
            ### RETURNS A LIST OF ONLY STRINGS ###

        elif obj == "<class 'tuple'>":
            #print("value:", value)
            value = value.lstrip("(").rstrip(")").split(", ")
            value = tuple([int(elem.replace("'","")) for elem in value])
        elif obj == "<class 'set'>":
            value = set(value)
        elif obj == "<class 'dict'>":
            value = value.lstrip("{").rstrip("}").split(", ")
            value = [elem.replace("'","").split(": ") for elem in value]
            value = dictFromList(value)

        dict_buffer[key] = value
        #print("End of dictFromList")
    return dict_buffer

def listFromList(list):
    #print("in func listFromList")
#    #print("Default:", DEFAULT_VAR_BANK)
    list_buffer = []
    for i in range(0, len(list)):
        #print("elem to convert", list[i])
        value = list[i][0] # returns a string which needs to be cast
        obj   = list[i][1] # data type of value
        #print("obj:", obj)

        if obj == "<class 'str'>":
            pass
        elif obj == "<class 'int'>":
            value = int(value)
        elif obj == "<class float>":
            #print("is float")
            value = float(value)
        elif obj == "<class 'bool'>":
            value = bool(value)
        elif obj == "<class 'list'>":
            #print("value:", value)
            value = value.lstrip("[").rstrip("]").split(", ")
            value = [elem.replace("'","").split(": ") for elem in value]
            value = listFromList(value)
            ### RETURNS A LIST OF ONLY STRINGS ###

        elif obj == "<class 'tuple'>":
            #print("value:", value)
            value = value.lstrip("(").rstrip(")").split(", ")
            value = tuple([int(elem.replace("'","")) for elem in value])
        elif obj == "<class 'set'>":
            value = set(value)
        elif obj == "<class 'dict'>":
            value = value.lstrip("{").rstrip("}").split(", ")
            value = [elem.replace("'","").split(": ") for elem in value]
            value = dictFromList(value)

        list_buffer.append(value)
        #print("list_buffer:", list_buffer)
        #print("End of listFromList")
    return list_buffer



################################## INPUT #####################################

def lowerTimeCheat(event):
    if var_bank['screen'] == "Game":
        var_bank['time'] = max(0, var_bank['time'] - 5)


def onKeyPress(event):
    # #print(event.char)
    global hand_dir
    if var_bank['screen'] == "Capturing Input":
        #print("remapping")
        settings[remapping] = event.keysym
        with open("Settings.txt", "r") as file:
            mappings = file.readlines()
        for i in range(len(mappings)):
            if remapping in mappings[i]:
                mappings[i] = (remapping + "=" + event.keysym + "="
                               + "<class 'str'>" + "\n")
                break
        with open("Settings.txt", "w") as file:
            file.writelines(mappings)
        label.destroy()
        showInfo()

    elif event.keysym == settings['boss key']:
        toggleBossKey()

    elif event.keysym == settings['pause']:
        #print("screen:", var_bank['screen'])
        # pauseGame()
        if var_bank['screen'] != "Pause Menu":
            pauseGame()
        elif var_bank['continuable']:
            continueGame()

    elif event.keysym == settings['drop']:
        if var_bank['screen'] == "Game":
            var_bank['state'] = "Falling"

    elif event.keysym == settings['left']:
        if hand_dir[0] != "L":
            hand_dir[1] = hand_dir[0]
            hand_dir[0] = "L"
    elif event.keysym == settings['right']:
        if hand_dir[0] != "R":
            hand_dir[1] = hand_dir[0]
            hand_dir[0] = "R"


def onKeyRelease(event):
    global hand_dir
    if event.keysym == "Left":
        if hand_dir[0] == "L":
            hand_dir[0] = hand_dir[1]
        else:
            hand_dir[1] = "_"
    if event.keysym == "Right":
        if hand_dir[0] == "R":
            hand_dir[0] = hand_dir[1]
        else:
            hand_dir[1] = "_"


################################# SCREENS ####################################

def pauseGame():
    global frame
    global GEAR_IMAGE
    # global var_bank
    #print("in function pauseGame, screen:", var_bank['screen'])
    if var_bank['screen'] == "Game":
        #print("Default:", DEFAULT_VAR_BANK)
        #print("var_bank:", var_bank)
        var_bank['ball_coords'] = canvas.coords(ball)
        var_bank['hand_coords'] = canvas.coords(hand)
        var_bank['goal_coords'] = canvas.coords(goal)
        #print("canvas coords:", type(canvas.coords(ball)))
        #print("Default:", DEFAULT_VAR_BANK)
        #print("var_bank:", var_bank)
        saveGame()
        # score_disp.destroy()
    if var_bank['screen'] == "Info":
        frame.destroy()
        back_button.destroy()
        #print("here")
    if var_bank['screen'] == "Win":
        frame.destroy()
        back_button.destroy()
    var_bank['screen'] = "Pause Menu"
    canvas.delete("all")

    frame = Frame(canvas, bg=GREEN)
    frame['borderwidth'] = 4
    frame['relief'] = 'solid'

    button1 = Button(frame, BUTTON_STYLE, text="Continue Game",
                     command=continueGame)
    if var_bank['continuable']:
        button1.configure(state="normal")
    else:
        button1.configure(state="disabled")

    button2 = Button(frame, BUTTON_STYLE, text="New Game",
                     command=newGame)
    button3 = Button(frame, relief="solid", borderwidth=4, width=300, bg=BLUE,
                     activebackground=PINK, font=("Terminal", 16),
                     image=GEAR_IMAGE, command=showInfo)

    global leaderboard
    leaderboard = Frame(canvas, width=10, bg=PINK)
    leaderboard['borderwidth'] = 4
    leaderboard['relief'] = 'solid'
    top_scores = []
    with open("Leaderboard.txt", "r") as file:
        for i in range(3):
            top_scores.append(file.readline().split(", "))

    # leaderboard_rows = []
    label = Label(leaderboard, font=("Terminal", 16), text="Leaderboard",
                  bg=PINK)
    label.grid(row=0)
    for row_pos, info in enumerate(top_scores, 1):
        length = len(info[1])
        text = f'{info[0]:9}' + " " * (10 - length) + info[1]
        label = Label(leaderboard, height=3, width=20, bg=BLUE, text=text,
                      font=("Terminal", 16), relief="solid", borderwidth=4)
        label.grid(row=row_pos, pady=10, padx=20)

    canvas.pack(fill=tk.BOTH, expand=True)
    frame.pack(side=tk.BOTTOM, ipady=10, pady=80)
    leaderboard.pack(side=tk.RIGHT, padx=20, anchor=tk.E)
    button1.grid(row=0, pady=10, padx=20)
    button2.grid(row=1, pady=10, padx=20)
    button3.grid(row=2, pady=10, padx=20)

    #print("end pauseGame")
#    #print("Default:", DEFAULT_VAR_BANK)
    window.update()


def newGame():
    global var_bank
    frame.destroy()
    leaderboard.destroy()
    var_bank = deepcopy(DEFAULT_VAR_BANK)
    # var_bank['ball_coords'] = DEFAULT_VAR_BANK['ball_coords']
    #print("in newGame")
#    #print("Default:", DEFAULT_VAR_BANK)
    var_bank['continuable'] = True
    #print("var_bank:", var_bank)
    generateLevel()
    gameLoop(time(), var_bank['vel_x'], var_bank['vel_y'])


def continueGame():
    #print("in func continueGame")
    if var_bank['screen'] == "Pause Menu":
        frame.destroy()
        leaderboard.destroy()
    #print("Here before loadGame")
    loadGame()
    #print("Here past loadGame")
    generateLevel()
    gameLoop(time(), var_bank['vel_x'], var_bank['vel_y'])


def showInfo():
    global frame
    global back_button
    frame.destroy()
    leaderboard.destroy()

    var_bank['screen'] = "Info"
    back_button = Button(canvas, BUTTON_STYLE, text="Back", command=pauseGame)
    back_button.pack(side=tk.TOP, anchor=tk.NW, padx=10, pady=10)

    frame = Frame(canvas, bg=PINK, width=800, height=600, borderwidth=4,
                  relief="solid")
    frame.pack(pady=40, anchor=tk.CENTER, ipady=5)

    gameCreditText =(" -  designed and created by Maarij Khan as part of a "
                   + "coursework for the University of Manchester")
    text1 = Label(frame, font=("Terminal", 16, "bold"), bg=GREEN, anchor=tk.W,
                  text="Credits")
    text2 = Label(frame, font=("Terminal", 12), bg=GREEN, anchor=tk.W,
                  text="Game")
    text3 = Label(frame, font=("Terminal", 12), bg=GREEN, anchor=tk.W,
                  text=gameCreditText)
    text4 = Label(frame, font=("Terminal", 12), bg=GREEN, anchor=tk.W,
                  text="Gear Icon in Information and Settings Button")
    text5 = Label(frame, font=("Terminal", 12), bg=GREEN, anchor=tk.W,
                  text=" -  created by Phoenix Group - Flaticon")
    text6 = Label(frame, font=("Terminal", 12), bg=GREEN, anchor=tk.W,
                  text="Hand Icon to drop ball")
    text7 = Label(frame, font=("Terminal", 12), bg=GREEN, anchor=tk.W,
                  text=" -  created by Maarij Khan")
    frame1 = Frame(frame, bg=PINK, width=800, height=600, relief="solid")

    # map_buttons = []
    # print(settings)
    # for key in settings:
    #     map_buttons.append(Button(frame1, BUTTON_STYLE, text="Remap " + key,
    #                                command=lambda:remap(key)))
    pause_map    = Button(frame1, BUTTON_STYLE, text="Remap Pause",
                          command=lambda:remap('pause'))
    drop_map     = Button(frame1, BUTTON_STYLE, text="Remap Drop",
                          command=lambda:remap('drop'))
    left_map     = Button(frame1, BUTTON_STYLE, text="Remap Left",
                          command=lambda:remap('left'))
    right_map    = Button(frame1, BUTTON_STYLE, text="Remap Right",
                          command=lambda:remap('right'))
    boss_key_map = Button(frame1, BUTTON_STYLE, text="Remap Boss Key",
                          command=lambda:remap('boss key'))

    # text1.grid(row=0, column=0, padx=10, pady=10, sticky=tk.W)
    text1.pack(ipadx=5, ipady=5, anchor=tk.W, padx=10, fill=tk.X, pady=10)
    text2.pack(ipadx=5, ipady=2, anchor=tk.W, padx=10, fill=tk.X)
    text3.pack(ipadx=5, ipady=2, anchor=tk.W, padx=10, fill=tk.X)
    text4.pack(ipadx=5, ipady=2, anchor=tk.W, padx=10, fill=tk.X)
    text5.pack(ipadx=5, ipady=2, anchor=tk.W, padx=10, fill=tk.X)
    text6.pack(ipadx=5, ipady=2, anchor=tk.W, padx=10, fill=tk.X)
    text7.pack(ipadx=5, ipady=2, anchor=tk.W, padx=10, fill=tk.X)
    frame1.pack()
    # for button in map_buttons:
    #     button.pack(pady=5)
    pause_map.pack(pady=5)
    drop_map.pack(pady=5)
    left_map.pack(pady=5)
    right_map.pack(pady=5)
    boss_key_map.pack(pady=5)


def remap(dict_key):
    global label
    global remapping
    frame.destroy()
    back_button.destroy()
    var_bank['screen'] = "Capturing Input"
    label = Label(canvas, font=("Terminal", 32), text="Capturing Input...",
                  bg=CREAM)
    label.pack(pady=100)
    remapping = dict_key


def toggleBossKey():
    global var_bank
    #print("in func toggleBossKey")
    if var_bank['screen'] != "Boss Key Screen":
        if var_bank['screen'] == "Game":
            # #print("ball_coords type:", type(var_bank['ball_coords']))
            var_bank['ball_coords'] = canvas.coords(ball)
            var_bank['hand_coords'] = canvas.coords(hand)
            var_bank['goal_coords'] = canvas.coords(goal)
            # #print("Default:", DEFAULT_VAR_BANK)
            saveGame()
            canvas.delete("all")
            # score_disp.destroy()
        # #print(canvas)
        elif var_bank['screen'] == "Win":
            frame.destroy()
        elif var_bank['screen'] == "Pause Menu":
            frame.destroy()
            leaderboard.destroy()
        var_bank['screen'] = "Boss Key Screen"
        canvas.create_image(0, 0, anchor=tk.NW, image=BOSS_KEY_IMAGE)
    else:
        canvas.delete("all")
        pauseGame()


def circArgs(x1, y1):
    return (x1, y1, x1+80, y1+80, {'fill':"white",'width':2})


def generateLevel():
    #print("in func generateLevel")
    var_bank['screen'] = "Game"
    global ball
    global chunks
    global hand
    global score_disp
    global goal

    score_disp = canvas.create_text(1400, 24, font=("Terminal", 24),
                                    text="Score: ")
    # score_disp.pack(pady=10, padx=10, side=tk.TOP, anchor=tk.NE)

    chunks = [[],[]]

    for i in range(8):
        chunks[0].append(canvas.create_oval(circArgs(200 * i + 60, 200)))
    for i in range(7):
        chunks[0].append(canvas.create_oval(circArgs(200 * i + 160, 350)))
    for i in range(8):
        chunks[1].append(canvas.create_oval(circArgs(200 * i + 60, 500)))

    # global circ_row
    # circ_row = [[],[],[]]
    # for i in range(8):
    #     circ_row[0].append(canvas.create_oval(circArgs(200 * i + 60, 200)))
    # for i in range(7):
    #     circ_row[1].append(canvas.create_oval(circArgs(200 * i + 160, 350)))
    # for i in range(8):
    #     circ_row[2].append(canvas.create_oval(circArgs(200 * i + 60, 500)))

    #print("Here")
    #print("var_bank:", var_bank)

    xy = var_bank['hand_coords']
    hand = canvas.create_image(xy, image=HAND_IMAGE)

    xy = var_bank['goal_coords']
    goal = canvas.create_image(xy, image=GOAL_IMAGE)

    xy = tuple(var_bank['ball_coords'])
    #print("xy type:", type(xy))
    # #print("Here", xy)
    ball = canvas.create_oval(xy, fill="red", width=2, tags="ball")
    # canvas.pack(fill=BOTH, expand=True)
    return


def gameLoop(start_time, vel_x, vel_y):
    # #print("in func gameLoop")
    global ball
    hand_vel = 0
    hand_coords = canvas.coords(hand)
    if hand_dir[0] == "L":
        hand_vel -= 200
    elif hand_dir[0] == "R":
        hand_vel += 200

    goal_vel = var_bank['goal_vel']
    goal_coords = canvas.bbox(goal)
    try:
        if goal_coords[R] > screen_width - EPSILON or goal_coords[L] < EPSILON:
            goal_vel *= -1
    except TypeError: # canvas items have been deleted
        pass
    var_bank['goal_vel'] = goal_vel

    ball_coords = canvas.coords(ball)
    try:
        ball_centre = (
                        (ball_coords[L] + ball_coords[R]) / 2,
                        (ball_coords[T] + ball_coords[B]) / 2
                      )
    except IndexError:
        return
        # #print("canvas coords:", canvas.coords(ball))
    if var_bank['state'] == "Falling":

        # Collide with walls

        if ball_coords[B] > screen_height - EPSILON:
            # canvas.move(ball, 0, screen_height - ball_coords[B] - EPSILON)
            # vel_y += -1.8 * vel_y #+ 10 * (ball_coords[B] - screen_height)
            canvas.delete(ball)
            ball_coords = [hand_coords[L] + OFFSET_X,
                               hand_coords[T] + OFFSET_Y,
                              hand_coords[L] + OFFSET_X + 2 * ball_radius,
                               hand_coords[T] + OFFSET_Y + 2 * ball_radius]
            ball = canvas.create_oval(ball_coords, fill="red", width=2)
            vel_x = 0
            vel_y = 0
            var_bank['state'] = "Held"
            #print("State:", var_bank['state'])
        if ball_coords[R] > screen_width - EPSILON:
            canvas.move(ball, screen_width - ball_coords[R] - EPSILON, 0)
            vel_x += -1.8 * vel_x
        elif ball_coords[L] < EPSILON:
            canvas.move(ball, ball_coords[L] + EPSILON, 0)
            vel_x += -1.8 * vel_x

        # Collide with circles

        # #print("row:", (ball_coords[T]-150)//150)
        # for circ in circ_row[max(int((ball_coords[T]-150)/150), 0)]:
        if ball_coords[T] < 450:
            chunk = 0
        else:
            chunk = 1
        for circ in chunks[chunk]:
            circ_coords = canvas.coords(circ)
            circ_centre = (
                           (circ_coords[L] + circ_coords[R]) / 2,
                           (circ_coords[T] + circ_coords[B]) / 2
                          )
            circ_radius =  (circ_coords[R] - circ_coords[L]) / 2

            dist_sqr =      ( (ball_centre[X] - circ_centre[X]) ** 2
                           + (ball_centre[Y] - circ_centre[Y]) ** 2 )

            dist_sqr_to_collide = (ball_radius + circ_radius + EPSILON) ** 2
            # #print("dist:", type(dist))
            if dist_sqr < dist_sqr_to_collide:
                #print("ball:", ball_radius, "circ:", circ_radius)
                #print("dist sqr   :", dist_sqr)
                #print("dist to col:", dist_sqr_to_collide)

                # EITHER
                dist = vect_minus(circ_centre, ball_centre) # Vector is a list
                #print("dist:", dist)

                # Resolved velocity to normal
                multiplier = abs((vect_dot([vel_x,vel_y], dist))
                                 / vect_mag(dist))
                if multiplier > 0:
                    delta_vel = scal_x_vect(BOUNCINESS * multiplier,
                                            vect_normalise(dist))
                # else:
                #     #print("sticking")
                # delta_vel = scal_x_vect(BOUNCINESS * multiplier,
                #                           vect_normalise(dist))
                vel_x, vel_y = vect_minus([vel_x, vel_y], delta_vel)

                # OR
                # dist = vect_minus(ball_centre, circ_centre)
                # vel = vect_add(vel, collide(velocity, dist))

        # Collide with goal

        # #print("goal_coords:", goal_coords)
        overlapping = canvas.find_overlapping(goal_coords[0],
                                              goal_coords[1],
                                              goal_coords[2],
                                              goal_coords[3])
        if ball in overlapping:
            #print("overlapping:", overlapping)
            #print(ball_coords)
            onWin()

        # Freefall

        vel_y += GRAVITY
        # #print("vel:", vel)
        var_bank['vel_x'] = vel_x
        var_bank['vel_y'] = vel_y

    else: # Not in freefall
        vel_x = hand_vel
        vel_y = 0

    curr_time = time()

    # EITHER
    delta = curr_time - start_time
    # #print("time:", var_bank['time'])
    var_bank['time'] += delta
    score = str(int(var_bank['time']))
    # #print("score:", score, type(score))
    canvas.itemconfigure(score_disp,
                         text="Score: " + " " * (5 - len(score)) + score)
                                                 # ^ all scores take same width

    # Keeps hand on screen
    hand_disp = hand_vel * delta
    if (hand_coords[L] + hand_disp > 0 and
        hand_coords[L] + hand_disp < screen_width):
            canvas.move(hand, hand_disp, 0)
    elif var_bank['state'] == "Held":
        vel_x = 0
    canvas.move(goal, goal_vel * delta, 0)

    ball_disp_x = delta * vel_x
    ball_disp_y = delta * vel_y

    canvas.move(ball, ball_disp_x, ball_disp_y)

    #print(hand_dir)
    #print((curr_time - start_time) * 1000, "ms")

    window.after(22, lambda : gameLoop(curr_time, vel_x, vel_y))


def onWin():
    global canvas
    global frame
    global back_button
    #print("in func onWin")
    #print("hand_coords:", hand_coords, type(hand_coords))
    #print("ball coords pre:", var_bank['ball_coords'])
    hand_coords = canvas.coords(hand)
    var_bank['hand_coords'] = canvas.coords(hand)
    var_bank['ball_coords'] = canvas.coords(ball)
    var_bank['goal_coords'] = canvas.coords(goal)
    var_bank['continuable'] = False
    var_bank['screen'] = "Win"
    # #print("Default:", DEFAULT_VAR_BANK)
    saveGame()

    canvas.delete("all")
    # score_disp.destroy()
    back_button = Button(canvas, BUTTON_STYLE, text="Menu", command=pauseGame)
    back_button.pack(side=tk.TOP, anchor=tk.NW, padx=10, pady=10)
    frame = Frame(canvas, bg=PINK)
    frame.pack(anchor=tk.CENTER, pady=80, ipadx=10)
    label = Label(frame, bg=PINK, text="You win", font=("Terminal", 32))
    label.pack(pady=10)

    i_score = int(var_bank['time'])
    s_score = str(i_score)
    
    label2 = Label(frame, bg=PINK, text="Score: " + s_score,
                   font=("Terminal", 32))
    label2.pack()

    label3 = Label(frame, bg=PINK, text="Enter initials:",
                   font=("Terminal", 32))
    label3.pack()

    entry  =  Entry(frame, bd=4, relief="solid", font=("Terminal", 32))
    entry.pack(ipady=4, pady=10, side=tk.LEFT, padx=4)

    submit = Button(frame, bd=4, relief="solid", font=("Terminal", 24),
                    text=">>", bg=GREEN, command=lambda:
                    handleInitials(entry.get(), i_score, s_score))
    submit.pack(side=tk.RIGHT, anchor=tk.SE, pady=10, padx=4)

def handleInitials(initials, i_score, s_score):
    #print("initials:", initials)
    if len(initials) > 3:
        messagebox.showerror(title="Length Error",
                             message="The initials need to be at most 3 "
                                    + "characters long")
    elif ", " in initials: # loadGame interprets ', ' as splittable
        messagebox.showerror(title="Character Error",
                              message="The string ', ' cannot be part of the "
                                    + "initials")
    else:
        with open("Leaderboard.txt", "r") as file:
            leaderboard_list = file.readlines()
        for i in range(len(leaderboard_list)):
            #print(leaderboard_list[i])
            leaderboard_list[i] = leaderboard_list[i].rstrip("\n").split(", ")
        for i in range(len(leaderboard_list)):
            if i_score < int(leaderboard_list[i][1]):
                leaderboard_list.insert(i, [initials, s_score])
                break
        for i in range(len(leaderboard_list)):
            leaderboard_list[i] = (leaderboard_list[i][0] + ", "
                                   + leaderboard_list[i][1] + "\n")
        #print(leaderboard_list)
        with open("Leaderboard.txt", "w") as file:
            file.writelines(leaderboard_list)
        pauseGame()



################################## MAIN ######################################

window = Tk()
window.geometry("1600x900")
window.title("")
window.update()
window.bind("<KeyRelease>", onKeyRelease)
window.bind("<KeyPress>", onKeyPress)
window.bind("<Shift-E>", lowerTimeCheat)

screen_width  = window.winfo_width()
screen_height = window.winfo_height()

#print("w x h:", screen_width, screen_height)
canvas = Canvas(window, width=screen_width, height=screen_height)
canvas.configure(bg=CREAM)
canvas.pack()

# #print(screen_width, screen_height)

BOSS_KEY_IMAGE = ImageTk.PhotoImage(file="Images/BossKeyScreen.png")

HAND_IMAGE = Image.open("Images/Hand.png")
HAND_IMAGE = HAND_IMAGE.resize((64,64), resample=Image.NEAREST)
HAND_IMAGE = ImageTk.PhotoImage(image=HAND_IMAGE)

GOAL_IMAGE = ImageTk.PhotoImage(file="Images/Goal.png")

GEAR_IMAGE = PhotoImage(file="Images/Gear.png")

loadSettings()
loadGame()
var_bank['screen'] = "Pause Menu"
# print(var_bank)
pauseGame()

window.mainloop()
